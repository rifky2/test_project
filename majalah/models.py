from django.db import models

class Majalah(models.Model):
    judul = models.CharField(max_length = 50)
    edisi = models.DateTimeField()
    jumlah_halaman = models.CharField(max_length = 12)
    cover_boy = models.CharField(max_length = 25)
    harga = models.DecimalField(max_digits = 8, decimal_places = 2)