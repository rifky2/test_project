from django.contrib import admin
from .models import Majalah

class MajalahAdmin(admin.ModelAdmin):
    list_display = ('judul', 'edisi', 'jumlah_halaman', 'cover_boy', 'harga')

admin.site.register(Majalah, MajalahAdmin)
