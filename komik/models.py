from django.db import models

class Komik(models.Model):
    judul = models.CharField(max_length = 20)
    cover = models.CharField(max_length = 100)
    tentang = models.CharField(max_length = 100)
    jumlah_halaman = models.CharField(max_length = 12)
    sinopsis = models.TextField()
    penerbit = models.CharField(max_length = 100)
    tahun_terbit = models.DateField()
    harga = models.DecimalField(max_digits = 8, decimal_places = 2)
