from django.contrib import admin
from .models import Komik

class KomikAdmin(admin.ModelAdmin):
    list_display = ('judul', 'cover', 'tentang', 'jumlah_halaman', 'sinopsis', 'penerbit', 'tahun_terbit', 'harga')

admin.site.register(Komik, KomikAdmin)