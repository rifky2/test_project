from django.db import models

class Buku(models.Model):
    judul = models.CharField(max_length = 100)
    penerbit = models.CharField(max_length = 100)
    author = models.CharField(max_length = 25)
    jumlah_halaman = models.CharField(max_length = 12)
    sinopsis = models.TextField()
    tahun_terbit = models.DateField()
    harga = models.DecimalField(max_digits = 8, decimal_places = 2)
