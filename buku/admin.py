from django.contrib import admin
from .models import Buku

class BukuAdmin(admin.ModelAdmin):
    list_display = ('judul', 'penerbit', 'author', 'jumlah_halaman', 'sinopsis', 'tahun_terbit', 'harga')

admin.site.register(Buku, BukuAdmin)